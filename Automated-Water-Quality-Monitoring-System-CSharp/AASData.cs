﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MonitoringSystemThesis
{
 [Serializable]
    class AASData
    {
        public string TIME { set; get; }
        public string DO { set; get; }
        public string PH { set; get; }
        public string TEMP { set; get; }
        public string FILE { set; get; }
        public string FILE1 { set; get; }
        public string FILENAME { set; get; }
    }
}
