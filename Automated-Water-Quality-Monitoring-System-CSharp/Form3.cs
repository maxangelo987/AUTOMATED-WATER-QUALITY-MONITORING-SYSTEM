﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Ports;

namespace MonitoringSystemThesis
{
    public partial class Form3 : Form
    {
        AASData st;
        public Form3()
        {
            InitializeComponent();
            this.button1.Enabled = false;
            this.tabControl1.TabPages.Remove(tbSendSMS);
        }

       public SerialPort port = new SerialPort();
        public SerialPort port1 = new SerialPort();
        public clsSMS objclsSMS = new clsSMS();
        public clsSMS objclsSMS1 = new clsSMS();
        ShortMessageCollection objShortMessageCollection = new ShortMessageCollection();
        ShortMessageCollection objShortMessageCollection1 = new ShortMessageCollection();

        public void process()
        {
            objclsSMS.ClosePort(this.port);
            serialPort3.Close();
            port.Close();
            port1.Close();
            st.FILENAME = "FILE";
            st.FILE = textBox1.Text;


            if (!Directory.Exists("c://Confidential/DesignProject2/FILE/"))
            {
                Directory.CreateDirectory("c://Confidential/DesignProject2/FILE/");
            }

            try
            {
                FileStream fs = new FileStream("c://Confidential/DesignProject2/FILE/" + st.FILENAME, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryFormatter b = new BinaryFormatter();
                b.Serialize(fs, st);
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (!Directory.Exists("c://Confidential/DesignProject2/FILENAME/"))
            {
                Directory.CreateDirectory("c://Confidential/DesignProject2/FILENAME/");
            }

            if (File.Exists("c://Confidential/DesignProject2/FILENAME/" + st.FILE))
            {
                MessageBox.Show("File Already Exist");
            }
            else
            {
                try
                {
                    FileStream fs = new FileStream("c://Confidential/DesignProject2/FILENAME/" + st.FILE, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    BinaryFormatter b = new BinaryFormatter();
                    b.Serialize(fs, st);
                    fs.Close();
                }

                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                StartRecording x = new StartRecording();           
                x.Show();           
            }

        }

        public void process1()
        {

            st.FILENAME = "FILE";
            st.FILE1 = textBox2.Text;


            if (!Directory.Exists("c://Confidential/DesignProject2/FILE1/"))
            {
                Directory.CreateDirectory("c://Confidential/DesignProject2/FILE1/");
            }

            try
            {
                FileStream fs = new FileStream("c://Confidential/DesignProject2/FILE1/" + st.FILENAME, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryFormatter b = new BinaryFormatter();
                b.Serialize(fs, st);
                fs.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            if (!File.Exists("c://Confidential/DesignProject2/FILENAME/" + st.FILE1))
            {
                MessageBox.Show("File Does'nt Exist");
            }
            else
            {
                ViewRecords x = new ViewRecords();
                x.Show();
            }

        }

        public static string portname = " ";

        private void button1_Click(object sender, EventArgs e)
        {
            st = new AASData();
            st.FILE = textBox1.Text;
            portname = cboPortName.Text;
            objclsSMS.ClosePort(this.port);

            if (textBox1.Text == "filename") {
                MessageBox.Show("Create another filename.");
            }

            else if (!serialPort3.IsOpen)
            {
                try
                {
                    serialPort3.PortName = cboPortName.Text;
                    serialPort3.Open();
                    process();
                }
                catch (Exception exe)
                {
                    MessageBox.Show(exe.Message);
                }
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            st = new AASData();
            st.FILE = textBox2.Text;

            process1();


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbPortSettings_Click(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            serialPort3.BaudRate = Convert.ToInt32(this.cboBaudRate.Text);
            serialPort3.PortName = this.cboPortName.Text;

            try
            {
                //Open communication port 
                this.port = objclsSMS.OpenPort(this.cboPortName.Text, Convert.ToInt32(this.cboBaudRate.Text), Convert.ToInt32("8"), Convert.ToInt32("300"), Convert.ToInt32("300"));

                if (this.port != null)
                {
                 //  this.gboPortSettings.Enabled = false;
                    

                    //MessageBox.Show("Modem is connected at PORT " + this.cboPortName.Text);
                    this.statusBar1.Text = "Arduino is connected at PORT " + this.cboPortName.Text;

                 
                    this.lblConnectionStatus.Text = "Connected at " + this.cboPortName.Text;
                  //  this.btnDisconnect.Enabled = true;
               //     this.button1.Enabled = true;
                }

                else
                {
                    //MessageBox.Show("Invalid port settings");
                    this.statusBar1.Text = "Invalid port settings";
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }

        }

  

        #region Error Log
        public void ErrorLog(string Message)
        {
            StreamWriter sw = null;

            try
            {
                //WriteStatusBar(Message);

                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                //string sPathName = @"E:\";
                string sPathName = @"SMSapplicationErrorLog_";

                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string sErrorTime = sDay + "-" + sMonth + "-" + sYear;

                sw = new StreamWriter(sPathName + sErrorTime + ".txt", true);

                sw.WriteLine(sLogFormat + Message);
                sw.Flush();

            }
            catch (Exception ex)
            {
                //ErrorLog(ex.ToString());
            }
            finally
            {
                if (sw != null)
                {
                    sw.Dispose();
                    sw.Close();
                }
            }

        }
        #endregion

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                this.button1.Enabled = false;
                this.gboPortSettings.Enabled = true;
                objclsSMS.ClosePort(this.port);


                this.lblConnectionStatus.Text = "Not Connected";
           //     this.btnDisconnect.Enabled = false;

            }

            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }

        public static string portn = "";
        public static string bdr = "";
        public static string mbn = "";
       
        
        private void button3_Click(object sender, EventArgs e)
        {
            portn = comboBox2.Text;
            bdr = comboBox1.Text;

            try
            {
                //Open communication port 
                this.port1 = objclsSMS1.OpenPort(this.comboBox2.Text, Convert.ToInt32(this.comboBox1.Text), Convert.ToInt32("8"), Convert.ToInt32("300"), Convert.ToInt32("300"));

                if (this.port1!= null)
                {
                    this.tabControl1.Enabled = true;


                    //MessageBox.Show("Modem is connected at PORT " + this.cboPortName.Text);
                    this.statusBar2.Text = "Modem is connected at PORT " + this.comboBox2.Text;
               
                    this.tabControl1.TabPages.Add(tbSendSMS);
                    this.label5.Text = "Connected at " + this.comboBox2.Text;
              //      this.button4.Enabled = true;
                    this.button1.Enabled = true;

                }

                else
                {
                    //MessageBox.Show("Invalid port settings");
                    this.statusBar2.Text = "Invalid port settings";
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message);
            }
        }

    
    

        private void btnSendSMS_Click_1(object sender, EventArgs e)
        {
            mbn = this.txtSIM.Text;
            //.............................................. Send SMS ....................................................
            try
            {

                if (objclsSMS1.sendMsg(this.port1, this.txtSIM.Text, this.txtMessage.Text))
                {
                    //MessageBox.Show("Message has sent successfully");
                    //  this.statusBar1.Text = "Message has sent successfully";
                    MessageBox.Show("Message has sent successfully");
                }
                else
                {
                    //MessageBox.Show("Failed to send message");
                    // this.statusBar1.Text = "Failed to send message";
                    MessageBox.Show("Failed to send message");
                }

            }
            catch (Exception exe)
            {
                MessageBox.Show(exe.Message);
            }
        }
    }

}