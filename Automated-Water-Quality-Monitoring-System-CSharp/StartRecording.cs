﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.IO.Ports;

namespace MonitoringSystemThesis
{
    public partial class StartRecording : Form
    {

        public int current_hour = 0;
        int index = 0;
        long x = 1;

        public StartRecording()
        {

            current_hour = 0;
            InitializeComponent();

            serialPort1.BaudRate = 9600;
            serialPort1.PortName = Form3.portname;

            timer1.Start();



            Values = new ChartValues<ObservableValue>
            {

            };
            cartesianChart1.LegendLocation = LegendLocation.Right;

            Values1 = new ChartValues<ObservableValue>
            {

            };
            cartesianChart2.LegendLocation = LegendLocation.Right;

            Values2 = new ChartValues<ObservableValue>
            {

            };
            cartesianChart3.LegendLocation = LegendLocation.Right;

            try
            {
                //Open communication port 
                this.port1 = objclsSMS1.OpenPort(Form3.portn, Convert.ToInt32(Form3.bdr), Convert.ToInt32("8"), Convert.ToInt32("300"), Convert.ToInt32("300"));

                if (this.port1 != null)
                {


                }
                else
                {
                    //MessageBox.Show("Invalid port settings");
                    MessageBox.Show("Invalid port settings");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("e" + ex.Message);
                port1.Close();

            }

        }


        //SerialPort port = new SerialPort();
        SerialPort port1 = new SerialPort();
        clsSMS objclsSMS = new clsSMS();
        clsSMS objclsSMS1 = new clsSMS();
        ShortMessageCollection objShortMessageCollection = new ShortMessageCollection();
        ShortMessageCollection objShortMessageCollection1 = new ShortMessageCollection();

        public ChartValues<ObservableValue> Values
        {
            get;
            set;
        }
        public ChartValues<ObservableValue> Values1
        {
            get;
            set;
        }
        public ChartValues<ObservableValue> Values2
        {
            get;
            set;
        }

        private void checkTime()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("d" + ex.Message);

            }
        }

        private void btn_turnoff_Click(object sender, EventArgs e)
        {

        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void StartRecording_Load(object sender, EventArgs e)
        {
            cartesianChart1.Series.Add(new LineSeries
            {
                Values = Values,
                StrokeThickness = 4,
                PointGeometrySize = 0,
                DataLabels = false
            });

            cartesianChart2.Series.Add(new LineSeries
            {
                Values = Values1,
                StrokeThickness = 4,
                PointGeometrySize = 0,
                DataLabels = false
            });

            cartesianChart3.Series.Add(new LineSeries
            {
                Values = Values2,
                StrokeThickness = 4,
                PointGeometrySize = 0,
                DataLabels = false
            });

        }

        AASData st;
        //  private object objclsSMS1;
        int fl = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // System.Threading.Thread.Sleep(6000);
            st = new AASData();

            float datetime = (((float)DateTime.Now.Hour * 10000) + ((float)DateTime.Now.Minute * 100)) + ((float)DateTime.Now.Second * 1);
            this.lbl_time.Text = datetime.ToString();

            if (!serialPort1.IsOpen)
            {
                try
                {
                    serialPort1.Open();
                }
                catch (Exception exe)
                {
                    MessageBox.Show("c" + exe.Message);

                }
            }

            lbl_cstate1.Text = serialPort1.ReadLine();
            lbl_cstate2.Text = serialPort1.ReadLine();
            lbl_cstate3.Text = serialPort1.ReadLine();

            string[] row0 = {
    lbl_time.Text,
    lbl_cstate1.Text,
    lbl_cstate2.Text,
    lbl_cstate3.Text
   };

            dataGridView1.Rows.Add(row0);
            dataGridView1.CurrentCell = dataGridView1.Rows[index].Cells[0];
            index++;
            System.Threading.Thread.Sleep(1000);

            try
            {
                FileStream fs = new FileStream("c://Confidential/DesignProject2/FILE/" + "FILE", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                BinaryFormatter b = new BinaryFormatter();
                st = (AASData)b.Deserialize(fs);
                fs.Close();


            }
            catch (Exception exe)
            {
                MessageBox.Show("a" + exe.Message);

            }


            if (!Directory.Exists("c://Confidential/DesignProject2/"))
            {
                Directory.CreateDirectory("c://Confidential/DesignProject2/");
            }

            try
            {
                if (File.Exists("c://Confidential/DesignProject2/" + st.FILE + x))
                {
                    MessageBox.Show("File Already exist");
                }
                else
                {
                    st.TIME = lbl_time.Text;
                    st.DO = lbl_cstate1.Text;
                    st.PH = lbl_cstate2.Text;
                    st.TEMP = lbl_cstate3.Text;

                    float flt1 = float.Parse(st.DO);
                    float flt2 = float.Parse(st.PH);
                    float flt3 = float.Parse(st.TEMP);

                    Values.Add(new ObservableValue(flt1));
                    Values1.Add(new ObservableValue(flt2));
                    Values2.Add(new ObservableValue(flt3));

                    if (flt2 >= 8 && fl >= 30)
                    {
                        sendmessage("High PH Detected");
                        fl = 0;

                    }
                    else if (flt2 < 7 && fl >= 30)
                    {
                        sendmessage("Low PH Detected");
                        fl = 0;

                    }
                    fl++;


                    FileStream fs = new FileStream("c://Confidential/DesignProject2/" + st.FILE + x, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    x++;
                    BinaryFormatter b = new BinaryFormatter();
                    b.Serialize(fs, st);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("b" + ex.Message);

            }


        }


        public void sendmessage(string mes)
        {



            fl = 0;

            //.............................................. Send SMS ....................................................
            try
            {

                if (objclsSMS1.sendMsg(this.port1, Form3.mbn, mes))
                {
                    //MessageBox.Show("Message has sent successfully");
                    //  this.statusBar1.Text = "Message has sent successfully";
                    MessageBox.Show("Message has sent successfully");

                }
                else
                {
                    //MessageBox.Show("Failed to send message");
                    // this.statusBar1.Text = "Failed to send message";
                    MessageBox.Show("Failed to send message");

                }

            }
            catch (Exception exe)
            {
                MessageBox.Show("f" + exe.Message);

            }



        }

        private void ErrorLog(string v)
        {
            throw new NotImplementedException();
        }

        private void lbl_onoff_Click(object sender, EventArgs e)
        {

        }

        private void lbl_time_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

    }
}